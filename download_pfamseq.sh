#!/bin/bash 

base_url=ftp://ftp.ebi.ac.uk/pub/databases/Pfam/current_release
temp_files=()

check_newer() {
  remote_date=$(date --date="$(curl -s -I $1 | grep Last-Modified | cut -f2- -d\ )" +%s)
  file_date=$(stat --printf=%Y $2)
  if [ $remote_date -lt $file_date ]; then
    return 1
  else
    return 0
  fi
}

cleanup() {
  rm -f "${tempfiles[@]}"
}

download() {
  remote=$base_url/$1.gz
  file=$1
  if [ -e $file ]; then
    if check_newer $remote $file; then
      echo Remote version of $file looks newer: removing existing file
      rm $file
    fi
  fi
  if [ ! -e $file ]; then
    echo Downloading $file
    tempfiles+=( "$file.tmp.gz" )
    curl -s $remote > $file.tmp.gz
    gunzip $file.tmp.gz
    mv $file.tmp $file
  fi
}

trap cleanup 0

if [ ! $(which curl) ]; then
  echo Command "curl" not found
  exit 1
fi

if [ ! -d seqlib ]; then
  echo Creating directory pfamseq for pfamseq and uniprot fasta files
  mkdir seqlib
fi
cd seqlib

download pfamseq
download uniprot

